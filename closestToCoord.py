import mysql.connector
import geopy.distance
import math


cnx = mysql.connector.connect(user='user', password='password',
                              host='iptoserver',
                              database='xjobb', 
                              auth_plugin='mysql_native_password')

stopDictionary = {}
def inputIntoDB():
    inputvalues = []
    for key1, value1 in stopDictionary.items():
        for key2, value2 in value1.items():
            inputvalues.append((key1, key2 ,value2['region'], value2['id'], value2['dist'], value2['stopID'],  value2['id'].split("|")[1]))    
    print(inputvalues)    
    mycursor = cnx.cursor(buffered=True)
    sql = "INSERT INTO from_coordiantes_Searches (uuid, time, region, id,  dist, fromStopID, toStopID) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    mycursor.executemany(sql, inputvalues)
    cnx.commit()
    stopDictionary = {}

#
def getDistance():    
    startTime = 1563416479405
    endtime = 1571210942692
    jumps = 1000000
    cursor = cnx.cursor(buffered=True)
    for e in range(math.ceil((endtime-startTime)/jumps)):
        query="SELECT coorDi.uuid, coorDi.time, ST_X(coorDi.pointText) as latitude, ST_Y(coorDi.pointText) as longitude, region, id as searchID FROM mtusage2_searchonly_withpoint As coorDi where region = 1 and coorDi.time <= {} and coorDi.time >= {} ;".format(startTime+jumps, startTime)
        cursor.execute(query)
        searchQueries = cursor.fetchall()
        startTime = startTime+jumps
        query2 = "select id as stopID, ST_X(pointT) as latitudeStop, ST_Y(pointT)as longitudeStop, region as regionStop from mtstopareas_withPoints where region = 1"
        cursor.execute(query2)
        stopsQueries = cursor.fetchall()
        
        for (uuid, time, latitude, longitude, region, searchID) in searchQueries:
            coords_1 = (latitude, longitude)
            for (stopID, latitudeStop, longitudeStop, regionStop) in stopsQueries:
                coords_2 = (latitudeStop, longitudeStop)
                distanceOfStop = geopy.distance.distance(coords_1, coords_2).m
                if(uuid in stopDictionary):
                    if(time in stopDictionary[uuid]):
                        if(stopDictionary[uuid][time]["dist"] > distanceOfStop):
                            stopDictionary[uuid][time]["dist"]=distanceOfStop
                            stopDictionary[uuid][time]["stopID"]=stopID

                    else:

                        stopDictionary[uuid][time] = {
                                                    "region":regionStop,
                                                    "id":searchID,
                                                    "latitude": latitude,
                                                    "longitude": longitude,
                                                    "dist": distanceOfStop,
                                                    "stopID":stopID
                                                }
                else:
                    stopDictionary[uuid] = {time: {
                                                "region":regionStop,
                                                "id":searchID,
                                                "latitude": latitude,
                                                "longitude": longitude,
                                                "dist": distanceOfStop,
                                                "stopID":stopID
                                            }
                                        }
            print(len(stopDictionary))
    print(startTime)
    inputIntoDB()                               
                          

getDistance()