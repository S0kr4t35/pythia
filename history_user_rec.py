import os
import pandas as pd
import numpy as np
import sys
import csv
import time
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics import pairwise_distances
from scipy.sparse import csr_matrix
import subprocess as sp
import platform

# Confifure file paths, open and read training files
def configureData(routeIndex_filename, trainData_filename):
    # configure file path
    data_path = os.getcwd() + "/testdata/"
    print("Max allocation for system -> ",sys.maxsize)
    # Name of comma seperated values (csv) file holding the routes
    routes_filename = routeIndex_filename
     # Name of comma seperated values (csv) file holding the commuter route usage frequency
    usage_route_filename = trainData_filename
  
    df_stations = pd.read_csv(
        os.path.join(data_path, routes_filename),
        usecols=['routeID', 'toID', 'fromID'],
        dtype={'routeID': 'str', 'toID': 'int32', 'fromID':'int32'})
   
    df_commuters = pd.read_csv(
        os.path.join(data_path, usage_route_filename),
        usecols=['uuid', 'toID','routeID', 'usedTimes'],
        dtype={'uuid': 'str', 'toID': 'int32', 'usedTimes': 'int32', 'routeID' : 'str'})

    Mean = df_commuters.groupby(by="uuid",as_index=False)['usedTimes'].mean()
    Rating_avg = pd.merge(df_commuters,Mean,on='uuid')
    Rating_avg['adg_rating'] = Rating_avg['usedTimes_x']-Rating_avg['usedTimes_y']

    print("Allocating -> ", sys.getsizeof(Rating_avg))
    final=pd.pivot_table(Rating_avg,values='adg_rating',index='uuid',columns='routeID')
    final_route = final.fillna(final.mean(axis=0))
    # Replacing NaN by commuter Average
    final_user = final.apply(lambda row: row.fillna(row.mean()), axis=1)
    check = pd.pivot_table(Rating_avg,values='usedTimes_x',index='uuid',columns='routeID')

    b = cosine_similarity(final_user)
    np.fill_diagonal(b, 0)
    similarity_with_user = pd.DataFrame(b,index=final_user.index)
    similarity_with_user.columns=final_user.index

    return Rating_avg, similarity_with_user, df_stations

def clearScreen():
    if "Windows" in platform.system():
        sp.call('cls',shell=True)
    else:
        sp.call('clear',shell=True)

# Clear screen and print current progress.
def printStatus(resultDict, testLength, predTime):
    clearScreen()
    # No division of Zero
    if (len(resultDict) == 0):
        return 0;
    BADFAIL = -1;
    GOODFAIL = 0;
    successes = 0;
    totalTriesDone = 0;
    # Calculate tot successes
    for (key, val) in resultDict.items():
            if (key > 0):
             successes += val;
            totalTriesDone += val;

    print(  "Status of run: \nBad fail -> ", str(resultDict.get(BADFAIL)), "\n" + 
            "Success -> ", str(successes), "\n" + 
            "Good fail ->  " + str(resultDict.get(GOODFAIL)), "\n" + 
            "Tested: ", (totalTriesDone / testLength)*100, "%", "\n" + 
            "Prediction generated in (AVG) (s): ", (time.time() - predTime) / totalTriesDone,"\n" +
            str(resultDict))
            
# return similar users
def find_n_neighbours(df,n):
    order = np.argsort(df.values, axis=1)[:, :n]
    df = df.apply(lambda x: pd.Series(x.sort_values(ascending=False)
           .iloc[:n].index, 
          index=['top{}'.format(i) for i in range(1, n+1)]), axis=1)
    return df

# Return routes from similar users
def get_user_similar_routes(Rating_avg, routes, user1, user2):
    common_routes = Rating_avg[Rating_avg.uuid == user1].merge(
    Rating_avg[Rating_avg.uuid == user2],
    on = "routeID",
    how = "inner" )
    return common_routes.merge(routes, on = 'routeID')

# Open read and return a dictionary for verification / validation
def getTestDict(validateName):
    returnDict = {}
    with open(os.getcwd() + "/testdata/"+validateName, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        index = 0;
        for row in reader:
            returnDict[index] = {"uuid":row['uuid'], "routeID":row['routeID'], "toID":row['toID']}
            index +=1;
    return returnDict;
# Input route and user matrix returns outcome category code -1 to 5    
def makePrediction(tempRoute, similarUserMatrix, routes):
    foundOnTry = 1; # Keeps track on the amount of tries needed
    foundNeighbours = similarUserMatrix[similarUserMatrix.index==tempRoute['uuid']].values
    if (len(foundNeighbours) == 0):
        return GOODFAIL;
    for neighbour in foundNeighbours[0]:
            foundSimilarRoutes = get_user_similar_routes(Rating_avg, routes, tempRoute['uuid'], neighbour)
            foundSimilarRoutes = foundSimilarRoutes.loc[ : , ['toID']]
            if (len(foundSimilarRoutes) > 0):
                for prediction in foundSimilarRoutes.values[:5]:
                    # Check if prediction is accurate
                    if int(tempRoute['toID']) in prediction:
                        return foundOnTry;
                    foundOnTry += 1;
                    if foundOnTry == MAX_TRIES + 1:
                        return BADFAIL         
            if foundOnTry == MAX_TRIES + 1:
                return BADFAIL
    return BADFAIL

ROUTE_INDEX_FILENAME = '*.csv'
TRAINING_DATA_FILENAME = '*.csv'
VALIDATION_DATA_FILENAME = "*.csv"
started = time.time()

# Generate all matrixes
Rating_avg, similarity_with_user, routes = configureData(routeIndex_filename = ROUTE_INDEX_FILENAME, trainData_filename = TRAINING_DATA_FILENAME)
timeToBuiltModel = time.time() - started;
print("Success: Matrixes created in ", time.time() - started, " secs")
# Load dictionary to verify / validate against
verifyDict = getTestDict(VALIDATION_DATA_FILENAME)
# Starting testing  
print("Generating neighbour list")
user_similarity_matrix = find_n_neighbours(similarity_with_user,30)
predTimeStart = time.time()
# Outcome categories
MAX_TRIES = 5;
BADFAIL = -1;
GOODFAIL = 0;
# Keeps the score
resultDict = { 
            BADFAIL:0, 
            GOODFAIL:0, 
            1:0, 2:0, 3:0, 4:0, 5:0
            }

# Start predicting
for key, testRoute in verifyDict.items():
    resultDict[makePrediction(tempRoute = testRoute, similarUserMatrix= user_similarity_matrix, routes = routes)] +=1;
    #print(resultInt)
    printStatus(resultDict, len(verifyDict), predTimeStart)
print("\nModel creation time (s): ", timeToBuiltModel, "\nFINISHED!")