import subprocess
import sys
import os
import platform
try:
    import pandas as pd
    from scipy.sparse import csr_matrix
    import math
    import numpy as np
    import csv
    import time
    import gc
    # utils import
    from fuzzywuzzy import fuzz
    from sklearn.neighbors import NearestNeighbors
    # visualization imports
    import seaborn as sns
    import matplotlib.pyplot as plt
    from sklearn.metrics.pairwise import cosine_similarity
    plt.style.use('ggplot')

except(ImportError):
    print(platform.system())
    path = os.getcwd()
    if ("Windows" in platform.system()):
        path = path + "/x64pkg"
        subprocess.run("pip3 install " + path + "/numpy-1.17.3+mkl-cp38-cp38-win_amd64.whl", shell=True)
        subprocess.run("pip3 install " + path + "/scipy-1.3.1-cp38-cp38-win_amd64.whl", shell=True)
        subprocess.run("pip3 install " + path + "/scikit_learn-0.21.3-cp38-cp38-win_amd64.whl", shell=True)
        subprocess.run("pip3 install " + path + "/matplotlib-3.2.0rc1-cp38-cp38-win_amd64.whl", shell=True)
        subprocess.run("pip3 install seaborn", shell=True)
        subprocess.run("pip3 install pandas", shell=True)
        subprocess.run("pip3 install fuzzywuzzy", shell=True)
        
    else:
        process = subprocess.run("pip3 install -r " + path+"/requirements.txt", shell=True)
    print("RERUN PROGRAM NOW")
    exit()

def prepareData(stationThres, userActThres):
    # configure file path
    data_path = os.getcwd() + "/testdata/"
    print(data_path)
    
    stations_filename = 'commuter_route_id.csv'
    usedTimes_filename = 'ITEMTEST.csv'
    # read data
    df_stations = pd.read_csv(
        os.path.join(data_path, stations_filename),
        usecols=['routeID', 'toID', 'fromID', 'routeINT'],
        dtype={'routeID': 'str', 'toID': 'int32', 'fromID':'int32', 'routeINT':'int32'})

    df_users = pd.read_csv(
        os.path.join(data_path, usedTimes_filename),
        usecols=['uuid', 'toID','routeID', 'usedTimes', 'routeINT'],
        dtype={'uuid': 'str', 'toID': 'int32', 'usedTimes': 'int32', 'routeID' : 'str', 'routeINT':'int32'})

    print("Station info -->")
    print(df_stations.info())
    print("Unique stations -->" , len(df_stations.routeID.unique()))

    # No of ratings of how many table
    df_users_cnt_tmp = pd.DataFrame(df_users.groupby('usedTimes').size(), columns=['count'])
    print(df_users_cnt_tmp)
    # there are a lot more counts in rating of zero

    #---------USER INFO ---------€
    num_users = len(df_users.uuid.unique())
    num_items = len(df_stations.routeID.unique())
    print("User info --> ")
    print(df_users.info())
    print("Unique uuid:s --> ",  len(df_users.uuid.unique()))


    total_cnt = num_users * num_items
    rating_zero_cnt = total_cnt - df_users.shape[0]
    # append counts of zero rating to df_ratings_cnt
    df_users_cnt = df_users_cnt_tmp.append(
        pd.DataFrame({'count': rating_zero_cnt}, index=[0.0]),
        verify_integrity=True,
    ).sort_index()
    print("No of not rated stations --> \n", df_users_cnt)

    # add log count
    df_users_cnt['log_count'] = np.log(df_users_cnt['count'])
    print(df_users_cnt)

    ax = df_users_cnt[['count']].reset_index().rename(columns={'index': 'rating score'}).plot(
        x='rating score',
        y='count',
        kind='bar',
        figsize=(12, 8),
        title='Count for Each Rating Score (in Log Scale)',
        logy=True,
        fontsize=12,
    )
    ax.set_xlabel("Route usage")
    ax.set_ylabel("Used by number")
    fig = ax.get_figure()
    fig.savefig("output.png")

    # get rating frequency
    df_stations_cnt = pd.DataFrame(df_users.groupby('routeID').size(), columns=['count'])
    print(df_stations_cnt.head())

    # plot rating frequency of all movies
    ax = df_stations_cnt \
        .sort_values('count', ascending=False) \
        .reset_index(drop=True) \
        .plot(
            figsize=(12, 8),
            title='Rating Frequency of All routes',
            fontsize=12
        )
    ax.set_xlabel("routeID")
    ax.set_ylabel("number of ratings")
    fig = ax.get_figure()
    fig.savefig("output2.png")

    # plot rating frequency of all movies in log scale
    ax = df_stations_cnt \
        .sort_values('count', ascending=False) \
        .reset_index(drop=True) \
        .plot(
            figsize=(12, 8),
            title='Rating Frequency of All routes (in Log Scale)',
            fontsize=12,
            logy=True
        )
    ax.set_xlabel("routeID")
    ax.set_ylabel("number of ratings (log scale)")
    fig = ax.get_figure()
    fig.savefig("output_stationPopularity.png")
    print(df_stations_cnt['count'].quantile(np.arange(1, 0.6, -0.05)))
    print("TOTAL VOTES BY USER --> \n")
    df_users_cnt = pd.DataFrame(df_users.groupby('uuid').size(), columns=['count'])
    #print(df_users_cnt)
    # filter data
    station_rating_thres = stationThres;
    popular_stations = list(set(df_stations_cnt.query('count >= @station_rating_thres').index))
    station_filter = df_users.routeID.isin(popular_stations)
    ratings_thres = userActThres;
    active_users = list(set(df_users_cnt.query('count >= @ratings_thres').index))
    user_filter = df_users.uuid.isin(active_users).values;
    df_user_drop_users = df_users[user_filter & station_filter]
    #print('shape of original ratings data: ', df_users.shape)
    print('shape of ratings data after dropping both unpopular movies and inactive users: ', df_user_drop_users.shape)

    print("Filtering out commuters as voters > ", ratings_thres)
    print("Dropped --> ", len(active_users), " none commuters!")
    print("Dropped --> ", len(station_filter), " popular stations!");
    # Free prev allc mem
    del df_stations_cnt, df_users_cnt
    print("Will allocate -> " , sys.getsizeof(df_user_drop_users))

    df_user_drop_users.set_index(['routeINT', 'uuid', 'usedTimes'], append=True)
    print(np.where(df_user_drop_users.index.duplicated()))

    station_user_mat = df_user_drop_users.pivot_table(index='routeINT', columns='uuid', values='usedTimes').fillna(0)
    
    # transform matrix to scipy sparse matrix
    station_user_mat_sparse = csr_matrix(station_user_mat.values)
    
    
    itemlist = list(df_stations.set_index('routeINT').loc[station_user_mat.index].routeID)
    reverse_hashmap = {}
    hashmap = {}
    for x in range(len(itemlist)):
        hashmap[itemlist[x]] = x;
        reverse_hashmap[x] = itemlist[x];
    
    del df_users, df_user_drop_users, station_user_mat, df_stations
    gc.collect()
    return station_user_mat_sparse, hashmap, reverse_hashmap

def createModel(matrix_sparse, neighbours):
    JOBLIB_TEMP_FOLDER="/tmp"
    # define model
    model_knn = NearestNeighbors(metric='cosine', algorithm='brute', n_neighbors=neighbours, n_jobs=-1)
    model_knn.fit(matrix_sparse)
    return model_knn

# create mapper from movie title to index
def returnSuggestion(model, matrixData, noOfRecommends):
    # transform matrix to scipy sparse matrix
    distances, indices = model.kneighbors(matrixData, n_neighbors=noOfRecommends+1)
    # get list of raw idx of recommendations
    raw_recommends = \
        sorted(list(zip(indices.squeeze().tolist(), distances.squeeze().tolist())), key=lambda x: x[1])[:0:-1]
    return raw_recommends


def getTestDict(validateName):
    returnDict = {}
    with open(os.getcwd() + "/testdata/"+validateName, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        index = 0;
        for row in reader:
            returnDict[index] = {"from":row['fromID'], "to":row['toID'], "routeID":row['routeID']}
            index +=1;
    return returnDict;

def tryModel(mapper,knownFrom, knownTo):
    
    VALUE_ERROR = -2;
    NOTFOUND = -1;
    inputFromRoute = knownFrom
    inputTo = knownTo

    index = mapper.get(inputFromRoute, None);
    if (index == None):
        print("Route not in list! Fallback to other method");
        print(inputTo)
        return VALUE_ERROR, 0;
    inputMatrixdata = dataMatrix[index];
    t0 = time.time()
    predictionList = returnSuggestion(model, inputMatrixdata, 6);
    elapsed = time.time() - t0;
    predictedStations = []

    for tryNumber, (idx, dist) in enumerate(predictionList):
            
            #print('{0}: {1}, with distance of {2}'.format(i, inverseDataMapper[idx], dist))
            
            # Split the routes back to stations
            if (len(predictedStations) == 4):
                # Use destination stop of the last route.
                predictedStations.append(int(inverseDataMapper[idx].split("|")[1]))
        
            if (len(predictedStations) < 4):
                predictedStations.append(int(inverseDataMapper[idx].split("|")[0]))
                predictedStations.append(int(inverseDataMapper[idx].split("|")[1]))
 
            if (knownTo in predictedStations):
                print("Model found tostation in route -> ", tryNumber)
                return tryNumber, elapsed;
           
            # Speed shit up
            if (len(predictedStations) == 5):
                break;

    return NOTFOUND, 0;




# Do once (configure model)
# Fitt-variables 
station_usage_thres = 2;
user_activity_thres = 5;
no_of_neighbours = 4;

dataMatrix, dataMapper, inverseDataMapper = prepareData(station_usage_thres, user_activity_thres); 

model = createModel(dataMatrix, no_of_neighbours);
validateName = "*.csv"
verifyDict = getTestDict(validateName);
notFound = 0;
found = 0;
cpuTotTime = 0.00;
triesTot = 0;
for x in range(len(verifyDict)):
    tempRoute = verifyDict.get(x)
    testFrom = tempRoute['routeID']
    testTo = int(tempRoute['to']) 
    score, cpuTime = tryModel(dataMapper, testFrom, testTo)
    # Write results (save bc pointer type)
    tempRoute['score'] = score;
    tempRoute['cputime'] = cpuTime;
    if (x % 100 == 0):
        print("{0:.2f}".format((x / len(verifyDict))*100), "% Done!")
    # Statistics 
    if (score == -1):
        notFound +=1;
    if (score > 0):
        found +=1;
        cpuTotTime += cpuTime;
        triesTot += score;
    
    

#print(verifyDict)
print("\nNOT FOUND ->", notFound)
print("\nFOUND ->", found)
print("\nTimetaken AVG: {:.2f}ms".format((cpuTotTime / found)*1000))
print("\nTries per prediction:", triesTot / found)

print("\nIn {:.2f} % we needed {:.2f} tries".format((found/(found + notFound))*100,(triesTot / found)))
