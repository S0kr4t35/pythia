import os
import csv
from collections import Counter
from collections import OrderedDict
def returnDict(filename):
    returnDict = {}
    print("Reading file...", filename)
    with open(os.getcwd() + "/testdata/"+filename, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        index = 0;
        for row in reader:
            returnDict[index] = int(row['toID'])
            index +=1;
    return returnDict;

def zero(trainDict, howmany):
    predicted = []
    print("Sorting most popular...")
    c = Counter(trainDict.values())  
    mostCommonStations = c.most_common(howmany)
    for x in range(len(mostCommonStations)):
        predicted.append(mostCommonStations[x][0])
    print("Most common stations in dataset ->", predicted)
    return predicted
    
def doTestZero(testDict, trainDict, noOfTries):
    predictions = zero(trainDict, noOfTries)
    predictions = predictions[:noOfTries]
    success = 0
    hitSteps = {0:0, 1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0, 10:0}
    for x in range(len(testDict)):
        if (x % 1000 == 0):
            print('{0:.2f}'.format((x / len(testDict))*100), "% done!")
        key = 0
        for val in predictions:
            if (testDict[x] == val):
                success += 1  
                hitSteps[key] += 1;
            key +=1;       
    print("Amount of successfull hits: ", success)
    print(hitSteps)
    return ((success / len(testDict))*100);

testDict = returnDict("*.csv")
trainDict = returnDict("*.csv")

successZero = {}
successZero[0] = doTestZero(testDict, trainDict, noOfTries=5)
totalZero = 0
for key, val in successZero.items():
    print("ZeroR test no: " + str(key) + "\n" +"procentSuccess: "+ str(val) + "\n")
    totalZero += val;
print("Total accuracy (zeroR): {0:.2f}".format((totalZero)))
print("Random 20 length: ", len(testDict))