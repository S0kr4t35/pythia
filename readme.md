# Pythia - Ask the oracle before the Journey  
## KNN-nearest neighbour recommender  
# Code cleanup 2019-12-13  
This is the repo containing the code used in the thesis work 'Creating a model for providing personal travel recommendations'. To read more about the reasearch and the reasearched issue go to 'The conducted research' paragraph below.  
### File contents of repo  
./history_user_rec.py <-- Main file building the model for predictions and running the verification / validation.   
./Alphabetic_method.py <-- Script calculating the number of inputs needed to find a station by alphabetic retrieval.  
./mlBaseline.py <-- Script implementing a zero-rule algorithm acting as the baseline for the study.  
./closestToCoordinate.py <-- Script used to map positional searches to the closest stations.  
./x64pkg/* <-- Python packages needed for winows 10 x64 with python 3.8.  
./tesdata/ <-- Direcotory for placement of test and train data.  
./route_item_rec.py <-- Experimental item recommender created for evaluation to adress the cold start part of problem.  
./requirements.txt <-- Pip packages needed for the main script to run.   

# Installation and running  
## On windows x64  
Run pip install on all packages under the directory of 'x64pkg'  
## On macOS / Unix  
Run pip install -r requirements.txt to install all listed dependancies from file.   

## Running  
Fill in missing attributes of the scripts and run.  

# The conducted research    

Abstract
Following the recent surge of implementations of various recommender systems, this study applied the technique of collaborative filtering to the area of commuting in the system of public transportation. This study processed and analyzed commuter search patterns collected from the planning mobile application MobiTime. The goal of this study was to find a method for providing accurate suggestions to commuter destinations. By the use of the method of collaborative filtering, this study trained a model on the commuter history that was validated on the data of commuters, by varying the presence of their routine routes the accuracy given a certain history prevalence was determined. The study found that the accuracy is varying with the history provided from the commuter and the significance of the result with the type of intended implementation. The overall performance of the created model was measured at a higher accuracy than suggesting the most popular alternatives. Providing more accurate alternatives to the commuter the results of this study could make the commuting experience more effective.  

The full report can be found [here].  

## Created by following this guide:  
https://towardsdatascience.com/prototyping-a-recommender-system-step-by-step-part-1-knn-item-based-collaborative-filtering-637969614ea  
  
    
## Code example :   
https://github.com/KevinLiao159/MyDataSciencePortfolio  